package com.admin.dj.djacm.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

import com.admin.dj.djacm.Adapter.TeamAdapter;
import com.admin.dj.djacm.Models.TeamModel;
import com.admin.dj.djacm.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeamFragment extends Fragment {


    public TeamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_team, container, false);

        Context context = getActivity();
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_team);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        List<TeamModel> teamModels = new ArrayList<>();



        TeamModel one = new TeamModel("Rushab Vincchi", "Chairperson",  R.drawable.four);
        teamModels.add(one);
        Log.i("Image id:", teamModels.toString());

        recyclerView.setAdapter(new TeamAdapter(context, teamModels));

        return view;
    }

}

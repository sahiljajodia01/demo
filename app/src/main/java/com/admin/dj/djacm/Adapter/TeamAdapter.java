package com.admin.dj.djacm.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.admin.dj.djacm.Models.TeamModel;
import com.admin.dj.djacm.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sahiljajodia on 10/01/18.
 */

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.MyViewHolder> {
    private Context context;
    private List<TeamModel> teamModels;

    public TeamAdapter(Context context, List<TeamModel> teamModels) {
        this.context = context;
        this.teamModels = teamModels;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView name;
        TextView post;


        MyViewHolder(View view) {
            super(view);
            photo = (ImageView) view.findViewById(R.id.person_photo);
            name = (TextView) view.findViewById(R.id.person_name);
            post = (TextView) view.findViewById(R.id.person_post);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.one_person, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(teamModels.get(position).getName());
        holder.post.setText(teamModels.get(position).getPost());
//        TeamModel teamModel = teamModels.get(position);
//        holder.photo.setImageResource(teamModel.getPhoto());

        Picasso.with(context).load(teamModels.get(position).getPhoto()).into(holder.photo);
    }

    @Override
    public int getItemCount() {
        return teamModels.size();
    }
}

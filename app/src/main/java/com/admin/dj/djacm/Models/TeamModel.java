package com.admin.dj.djacm.Models;

import android.graphics.drawable.Drawable;

import com.admin.dj.djacm.R;

/**
 * Created by sahiljajodia on 10/01/18.
 */

public class TeamModel {
    String name;
    String post;
    int photo;

    public TeamModel(String name, String post, int photo) {
        this.name = name;
        this.post = post;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public String getPost() {
        return post;
    }

    public int getPhoto() {
        return photo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
